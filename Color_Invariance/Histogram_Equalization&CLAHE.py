import cv2
import numpy as np
import argparse
from skimage.measure import compare_ssim as ssim
from skimage.measure import compare_mse as mse


def adjust_brightness(image, brightness=1.0):
    invgamma = 1.0 / brightness
    table = np.array([((i / 255.0) ** invgamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    return cv2.LUT(image, table)


def histogram_equalize(image, choice):
    if choice == 0:
        equalized = cv2.equalizeHist(image)
        return equalized
    else:
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        equalized = clahe.apply(image)
        return equalized


def meansquaredLoss(imageA, imageB):
    err = mse(imageA, imageB)
    return err


ap = argparse.ArgumentParser()
ap.add_argument("-b", "--bright", default=1, help="enter brightness")
ap.add_argument("-c", "--choice", default=0, help="Enter choice")
ap.add_argument("-p", "--path", required=True, help="enter Image path")
ap.add_argument("-s", "--space", default=0, help="Enter color space 0 - YCbCr, 1- LAB, 2-HSV")

args = vars(ap.parse_args())

brightness = float(args["bright"])
choice = int(args["choice"])
image = args["path"]
images = image.split(',')
c_space = int(args["space"])

space = {
    0: cv2.COLOR_BGR2YCrCb,
    1: cv2.COLOR_BGR2LAB
}

space_inverse = {
    0: cv2.COLOR_YCrCb2BGR,
    1: cv2.COLOR_LAB2BGR
}

original = cv2.imread(images[0])

brightened = cv2.imread(images[1]) if len(images) > 1 else adjust_brightness(original, brightness)

image_hcombine_1 = np.hstack([original, brightened])

original_new = cv2.cvtColor(original, space[c_space])
brightened_new = cv2.cvtColor(brightened, space[c_space])

original_new_split_light, original_new_split_ch1, original_new_split_ch2 = cv2.split(original_new)
brightened_new_split_light, brightened_new_split_ch1, brightened_new_split_ch2 = cv2.split(brightened_new)


equalized_original_light = histogram_equalize(original_new_split_light, choice)
equalized_original_ch1 = histogram_equalize(original_new_split_ch1, choice)
equalized_original_ch2 = histogram_equalize(original_new_split_ch2,choice)


equalized_brightened_light = histogram_equalize(brightened_new_split_light, choice)
equalized_brightened_ch1 = histogram_equalize(brightened_new_split_ch1, choice)
equalized_brightened_ch2 = histogram_equalize(brightened_new_split_ch2, choice)

equalized_original = cv2.merge((equalized_original_light, equalized_original_ch1, equalized_original_ch2))
equalized_brightened = cv2.merge((equalized_brightened_light, equalized_brightened_ch1, equalized_brightened_ch2))

equalized_original = cv2.cvtColor(equalized_original, space_inverse[c_space])
equalized_brightened = cv2.cvtColor(equalized_brightened, space_inverse[c_space])

image_hcombine_2 = np.hstack([equalized_original, equalized_brightened])
image_vcombined = np.vstack([image_hcombine_1, image_hcombine_2])

cv2.imshow('Images', image_vcombined)

print("\n------------------------------------------------------------------------------------")
print("\nMean squared Loss between original and light variant image : {}".format(meansquaredLoss(original, brightened)))
print("\nMean squared Loss between transformed original and light variant image : {}".format(
    meansquaredLoss(equalized_original, equalized_brightened)))
print("\nStructural Similarity between both original and light variant image : {}".format(
    ssim(original_new_split_light, brightened_new_split_light)))
print("\nStructural Similarity between both transformed original and light variant image : {}".format(
    ssim(equalized_original_light, equalized_brightened_light)))
print("\nMean squared error of other two channels of original and light variant Image : {},{}".format(meansquaredLoss(original_new_split_ch1,brightened_new_split_ch1), meansquaredLoss(original_new_split_ch2,brightened_new_split_ch2)))
print("\nMean squared Loss of original Light channels : {}".format(meansquaredLoss(original_new_split_light,brightened_new_split_light)))
print("\nMean squared loss of equalized light channels : {}".format(meansquaredLoss(equalized_original_light,equalized_brightened_light)))
print("\n------------------------------------------------------------------------------------")
cv2.waitKey()
