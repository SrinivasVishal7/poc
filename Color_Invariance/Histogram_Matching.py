import argparse, os
import numpy as np
import cv2


def hist_match_self(source, reference):
    oldshape = source.shape
    source = source.ravel()
    reference = reference.ravel()
    s_values, bin_idx, s_counts = np.unique(source, return_inverse=True,
                                            return_counts=True)
    t_values, t_counts = np.unique(reference, return_counts=True)
    s_quantiles = np.cumsum(s_counts).astype(np.float64)
    s_quantiles /= s_quantiles[-1]
    t_quantiles = np.cumsum(t_counts).astype(np.float64)
    t_quantiles /= t_quantiles[-1]

    interp_t_values = np.interp(s_quantiles, t_quantiles, t_values)

    return interp_t_values[bin_idx].reshape(oldshape)


def hist_match_rioHist(source, reference):
    orig_shape = source.shape
    source = source.ravel()

    if np.ma.is_masked(reference):
        reference = reference.compressed()
    else:
        reference = reference.ravel()

    s_values, s_idx, s_counts = np.unique(
        source, return_inverse=True, return_counts=True)
    r_values, r_counts = np.unique(reference, return_counts=True)
    s_size = source.size

    if np.ma.is_masked(source):
        mask_index = np.ma.where(s_values.mask)
        s_size = np.ma.where(s_idx != mask_index[0])[0].size
        s_values = s_values.compressed()
        s_counts = np.delete(s_counts, mask_index)

    s_quantiles = np.cumsum(s_counts).astype(np.float64) / s_size
    r_quantiles = np.cumsum(r_counts).astype(np.float64) / reference.size

    interp_r_values = np.interp(s_quantiles, r_quantiles, r_values)

    if np.ma.is_masked(source):
        interp_r_values = np.insert(interp_r_values, mask_index[0], source.fill_value)

    target = interp_r_values[s_idx]

    if np.ma.is_masked(source):
        target = np.ma.masked_where(s_idx == mask_index[0], target)
        target.fill_value = source.fill_value

    return target.reshape(orig_shape)


def hist_match_2(imsrc, imtint):
    nbr_bins = 255
    if len(imsrc.shape) < 3:
        imsrc = imsrc[:, :, np.newaxis]
        imtint = imtint[:, :, np.newaxis]
    imres = imsrc.copy()
    for d in range(imsrc.shape[2]):
        imhist, bins = np.histogram(imsrc[:, :, d].flatten(), nbr_bins, normed=True)
        tinthist, bins = np.histogram(imtint[:, :, d].flatten(), nbr_bins, normed=True)

        cdfsrc = imhist.cumsum()
        cdfsrc = (255 * cdfsrc / cdfsrc[-1]).astype(np.uint8)

        cdftint = tinthist.cumsum()
        cdftint = (255 * cdftint / cdftint[-1]).astype(np.uint8)
        im2 = np.interp(imsrc[:, :, d].flatten(), bins[:-1], cdfsrc)
        im3 = np.interp(im2, cdftint, bins[:-1])
        imres[:, :, d] = im3.reshape((imsrc.shape[0], imsrc.shape[1]))
    try:
        return imres
    except:
        return imres.reshape((imsrc.shape[0], imsrc.shape[1]))


parser = argparse.ArgumentParser()
parser.add_argument('--template_img', required=True)
parser.add_argument('--source_dir', required=True)
parser.add_argument('--output_dir', required=True)
parser.add_argument('--choice', default=0)
parser.add_argument('--cspace', default=None)
args = parser.parse_args()

choices = {
    0: hist_match_self,
    1: hist_match_rioHist,
    2: hist_match_2
}

template_img = cv2.imread(args.template_img)
cv2.imshow("template_image", template_img)

if not os.path.isdir(args.output_dir):
    os.makedirs(args.output_dir)

for fn in os.listdir(args.source_dir):
    source_img = cv2.imread(os.path.join(args.source_dir, fn))

    print(args.choice)

    if args.cspace is not None:
        space = cv2.COLOR_BGR2LAB
        inverseSpace = cv2.COLOR_LAB2BGR

        source_img = cv2.cvtColor(source_img, space)
        template_img = cv2.cvtColor(template_img, space)

        out_img = np.zeros_like(source_img)

        template_img_light, template_img_cr, template_img_cb = cv2.split(template_img)
        source_img_light, source_img_cr, source_img_cb = cv2.split(source_img)

        if int(args.choice) == 2:
            out_img = hist_match_2(source_img, template_img)
        else:
            out_img[:, :, 0] = choices[int(args.choice)](source_img_light, template_img_light)
            out_img[:, :, 1] = choices[int(args.choice)](source_img_cr, template_img_cr)
            out_img[:, :, 2] = choices[int(args.choice)](source_img_cb, template_img_cb)

        out_img = cv2.cvtColor(out_img, inverseSpace)

    else:
        out_img = np.zeros_like(source_img)

        if int(args.choice) == 2:
            out_img = hist_match_2(source_img, template_img)

        else:
            for c in range(3):
                out_img[:, :, c] = choices[int(args.choice)](source_img[:, :, c], template_img[:, :, c])

    GRAYFrame = cv2.cvtColor(out_img, cv2.COLOR_BGR2GRAY)
    GRAYFrame = cv2.GaussianBlur(GRAYFrame, (3, 3), 0)
    ret3, thresh = cv2.threshold(GRAYFrame, GRAYFrame[0][0], 255, cv2.THRESH_BINARY)
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
    out_img = cv2.bitwise_and(out_img, out_img, mask=closing)
    cv2.imwrite(os.path.join(args.output_dir, fn), out_img)

cv2.waitKey(0)
