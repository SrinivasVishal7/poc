from scipy import fftpack, ndimage
import matplotlib.pyplot as plt
import numpy as np
import cv2

image = cv2.imread('F371ncy.png')     # flatten=True gives a greyscale image
fft2 = fftpack.fft2(image)

plt.imshow(20*np.log10(abs(fft2)))
plt.show()