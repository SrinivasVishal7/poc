from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.stats import norm
import matplotlib.image as matimg
import glob
import cv2
from keras.layers import Input, Dense, Lambda
from keras.models import Model
from keras import backend as K
from keras import objectives

num_training_examples = 900
num_testing_examples = 100
image_vector_size = 150528

batch_size = 10
original_dim = 150528
latent_dim = 2
intermediate_dim = 512
nb_epoch = 500
epsilon_std = 1.0


def load_pill_images(path):
    img = matimg.imread(path)
    return img


def load_pill_local_images(path):
    paths = glob.glob(os.path.join(path + '/train', '*.bmp'))
    X_train = np.array([load_pill_images(p) for p in paths])

    paths = glob.glob(os.path.join(path + '/test', '*.bmp'))
    X_test = np.array([load_pill_images(p) for p in paths])

    return (X_train, X_test)


x = Input(batch_shape=(batch_size, original_dim))
h = Dense(intermediate_dim, activation='relu')(x)
z_mean = Dense(latent_dim)(h)
z_log_var = Dense(latent_dim)(h)


def sampling(args):
    (z_mean, z_log_var) = args
    epsilon = K.random_normal(shape=(batch_size, latent_dim), mean=0.,
                              )
    return z_mean + K.exp(z_log_var / 2) * epsilon



z = Lambda(sampling, output_shape=(latent_dim,))([z_mean, z_log_var])


decoder_h = Dense(intermediate_dim, activation='relu')
decoder_mean = Dense(original_dim, activation='sigmoid')
h_decoded = decoder_h(z)
x_decoded_mean = decoder_mean(h_decoded)


def vae_loss(x, x_decoded_mean):
    xent_loss = original_dim * objectives.binary_crossentropy(x,
                                                              x_decoded_mean)
    kl_loss = -0.5 * K.sum(1 + z_log_var - K.square(z_mean)
                           - K.exp(z_log_var), axis=-1)
    return xent_loss + kl_loss


vae = Model(x, x_decoded_mean)
vae.compile(optimizer='rmsprop', loss=vae_loss)


path = \
    '/home/hackathon/Desktop/Generative-Model-For-Image-Generation/db/'
(x_train, x_test) = load_pill_local_images(path)

x_train = x_train.astype('float32') / 255.
x_test = x_test.astype('float32') / 255.
print(len(x_test))
print("\n------------------")
print(len(x_train))
x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

vae.fit(
    x_train,
    x_train,
    shuffle=True,
    nb_epoch=nb_epoch,
    batch_size=batch_size,
    validation_data=(x_test, x_test),
)

# vae.load_weights('vae_pills.h5')

encoder = Model(x, z_mean)


decoder_input = Input(shape=(latent_dim,))
_h_decoded = decoder_h(decoder_input)
_x_decoded_mean = decoder_mean(_h_decoded)
generator = Model(decoder_input, _x_decoded_mean)


n = 10

# figure = np.zeros((face_size * n, face_size * n))

grid_x = np.linspace(-10, 10, 5)
grid_y = np.linspace(-10, 10, n)

pill_count = 0
for (i, xi) in enumerate(grid_x):
    for (j, yi) in enumerate(grid_y):
    	pill_count += 1
        z_sample = np.array([[yi, xi]])
        x_decoded = generator.predict(z_sample)
        x_decoded = x_decoded
        pill = x_decoded[0].reshape(224, 224, 3)
        cv2.imwrite('pill_image_{}.bmp'.format(pill_count),pill)



