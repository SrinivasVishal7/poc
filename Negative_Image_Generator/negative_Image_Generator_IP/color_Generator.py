import os
import cv2
import numpy as np
import argparse

# Constants
H_PLANE = 0
S_PLANE = 1
V_PLANE = 2

HUE_MIN = 0
HUE_MAX = 180
HUE_INTERVAL = 30

SAT_MIN = 0
SAT_MAX = 250
SAT_INTERVAL = 30

LIGHTNESS_MIN = 90
LIGHTNESS_MAX = 230
LIGHTNESS_INTERVAL = 5

original_images_parent_path = "E:/Work/0..DeepLearning/1.Tools/Tool_Creating_Dataset_Console" \
                              "/Tool_Creating_Dataset_Console/Negative_Image_Generator/Base_Images "
negative_images_parent_path = "E:/Work/0..DeepLearning/1.Tools/Tool_Creating_Dataset_Console" \
                              "/Tool_Creating_Dataset_Console/Negative_Image_Generator/Output_Images "

property = "color"


# Function to generate different colors
def Generate_Color_Variation_Images(Image_Input, Image_Thresh, plane, out_location, verbose=True):
    h_meanValue = 0
    s_meanValue = 0
    v_meanValue = 0
    count = 1
    condition = 0

    interval = 0
    min = 0
    max = 0

    if plane == V_PLANE:
        interval = LIGHTNESS_INTERVAL
        min = LIGHTNESS_MIN
        max = LIGHTNESS_MAX


    elif plane == H_PLANE:
        interval = HUE_INTERVAL
        min = HUE_MIN
        max = HUE_MAX


    elif plane == S_PLANE:
        interval = SAT_INTERVAL
        min = SAT_MIN
        max = SAT_MAX

    # Checking the input matrices
    if Image_Input is None or Image_Thresh is None:
        print("Error : Either of the matrix is empty")
        return False

    # bgr-hsv color conversion
    Image_HSV = cv2.cvtColor(Image_Input, cv2.COLOR_BGR2HSV)
    planes_HSV = cv2.split(Image_HSV)
    Image_Copy = planes_HSV[plane].copy();

    # Change in V => changes the lightness
    meanVal = cv2.mean(Image_HSV, mask=Image_Thresh)
    h_meanValue = meanVal[H_PLANE]
    s_meanValue = meanVal[S_PLANE]
    v_meanValue = meanVal[V_PLANE]

    if (verbose):
        print("Mean Val", meanVal)

    # Reducing lightness
    if plane == H_PLANE:
        condition = (h_meanValue - (count * interval))

    elif plane == S_PLANE:
        condition = (s_meanValue - (count * interval))

    elif plane == V_PLANE:
        condition = (v_meanValue - (count * interval))

    while condition > min:

        planes_HSV[plane] = cv2.subtract(Image_Copy, count * interval, mask=Image_Thresh)
        Image_HSV = cv2.merge(planes_HSV)
        Image_BGR = cv2.cvtColor(Image_HSV, cv2.COLOR_HSV2BGR)

        if plane == H_PLANE:
            fileLocation = os.path.sep.join([out_location,
                                             str(int(condition)) + "H_" + str(int(s_meanValue)) + "S_" + str(
                                                 int(v_meanValue)) + "V.png"])



        elif (plane == S_PLANE):
            fileLocation = os.path.sep.join([out_location,
                                             str(int(h_meanValue)) + "H_" + str(int(condition)) + "S_" + str(
                                                 int(v_meanValue)) + "V.png"])

        elif plane == V_PLANE:
            fileLocation = os.path.sep.join([out_location,
                                             str(int(h_meanValue)) + "H_" + str(int(s_meanValue)) + "S_" + str(
                                                 int(condition)) + "V.png"])

        if verbose:
            print(fileLocation)

        # Saving into disk
        cv2.imwrite(fileLocation, Image_BGR)
        count = count + 1

        # Recursive call
        if plane == H_PLANE:
            Generate_Color_Variation_Images(Image_BGR, Image_Thresh, S_PLANE, out_location)
            condition = (h_meanValue - (count * interval))


        elif (plane == S_PLANE):
            # Generate_Color_Variation_Images(Image_BGR, Image_Thresh, V_PLANE)
            condition = (s_meanValue - (count * interval))


        elif (plane == V_PLANE):
            condition = (v_meanValue - (count * interval))

    # Increasing lightness
    count = 1  # Resetting the count value
    if plane == H_PLANE:
        condition = (h_meanValue + (count * interval))

    elif plane == S_PLANE:
        condition = (s_meanValue + (count * interval))

    elif plane == V_PLANE:
        condition = (v_meanValue + (count * interval))

    while condition < max:

        planes_HSV[plane] = cv2.add(Image_Copy, count * interval, mask=Image_Thresh)
        Image_HSV = cv2.merge(planes_HSV)
        Image_BGR = cv2.cvtColor(Image_HSV, cv2.COLOR_HSV2BGR)

        if plane == H_PLANE:
            fileLocation = os.path.sep.join([out_location,
                                             str(int(condition)) + "H_" + str(int(s_meanValue)) + "S_" + str(
                                                 int(v_meanValue)) + "V.png"])

        elif plane == S_PLANE:
            fileLocation = os.path.sep.join([out_location,
                                             str(int(h_meanValue)) + "H_" + str(int(condition)) + "S_" + str(
                                                 int(v_meanValue)) + "V.png"])

        elif plane == V_PLANE:
            fileLocation = os.path.sep.join([out_location,
                                             str(int(h_meanValue)) + "H_" + str(int(s_meanValue)) + "S_" + str(
                                                 int(condition)) + "V.png"])

        if verbose:
            print(fileLocation)

        # Saving into disk
        cv2.imwrite(fileLocation, Image_BGR)
        count = count + 1

        # Recursive call
        if plane == H_PLANE:
            Generate_Color_Variation_Images(Image_BGR, Image_Thresh, S_PLANE, out_location)
            condition = (h_meanValue + (count * interval))

        elif plane == S_PLANE:
            # Generate_Color_Variation_Images(Image_BGR, Image_Thresh, V_PLANE)
            condition = (s_meanValue + (count * interval))

        elif plane == V_PLANE:
            condition = (v_meanValue + (count * interval))

    return True


def Call_Generate_Color_Variation_Images(input_image_path, output_image_path, prop):
    if not os.path.exists(input_image_path):
        print("Error : Input parent path is not found")
        return False

    for image_name in os.listdir(input_image_path):

        img_in = cv2.imread(os.path.join(input_image_path, image_name))
        image_name = image_name.split(".")[0]

        # Checking the input matrices
        if img_in is None:
            print("Error : Input matrix is empty")
            return False

        # Thresholding conversion
        img_gray = cv2.cvtColor(img_in, cv2.COLOR_BGR2GRAY)
        img_thresh = cv2.threshold(img_gray, 20, 255, cv2.THRESH_BINARY)[1]

        output_image_directory = os.path.sep.join([output_image_path, prop, image_name])

        if not os.path.exists(output_image_directory):
            os.makedirs(output_image_directory)

        if prop == "color":
            plane = H_PLANE
        else:
            plane = V_PLANE

        # For Color Variation, pass H_PLANE
        # For Light Variation, pass V_PLANE
        Generate_Color_Variation_Images(img_in, img_thresh, plane, output_image_directory)


ap = argparse.ArgumentParser()
ap.add_argument("-i", "--inputPath", default=None, required=True,
                help="path that contains the input images")
ap.add_argument("-o", "--outputPath", default=None, required=True,
                help="path to save the negative generated images(Size)")
ap.add_argument("-p", "--property", default=None, required=True,
                help="property : color or light")
args = vars(ap.parse_args())

Call_Generate_Color_Variation_Images(args["inputPath"], args["outputPath"], args["property"])

# Call_Generate_Color_Variation_Images(original_images_parent_path, negative_images_parent_path, property)
