import os
import cv2
import numpy as np
import string
import argparse
import random
from packages import negativeImprintGenerator

property = "imprint"

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--inputPath", default=None, required=True,
	help="path that contains the input images")
ap.add_argument("-o", "--outputPath", default=None, required=True,
	help="path to save the negative generated images(Imprint)")
ap.add_argument("-n", "--numberPill", default=10,
	help="maximum number of negative images")
args = vars(ap.parse_args())



# Function to generate different colors
def Generate_Imprint_Variation_Images(input_image_path, output_image_path, no_of_images, verbose=False):


	if not os.path.exists(input_image_path):
		print("Error : Input parent path is not found")
		return False
	

	for image_name in os.listdir(input_image_path):
	
		img = cv2.imread(os.path.join(input_image_path, image_name))
		image_name = image_name.split(".")[0]
		
		# Checking the input matrices
		if(img is None):
			print("Error : Input matrix is empty")
			return False
			
			
		NIG = negativeImprintGenerator (img, (6,8))
		
		
		output_image_directory = os.path.sep.join([output_image_path, property, image_name])
		if not os.path.exists(output_image_directory):
			os.makedirs(output_image_directory)
			print(output_image_directory)
	
	
	
		for img_count in range (no_of_images):
			outImage = NIG.getNegImage(image_name)
		
			if outImage is None and verbose==True:
				print (img_count)
		
			img_count += 1
			
			fileLocation = os.path.sep.join([output_image_directory, str(img_count) + ".png"])
			cv2.imwrite(fileLocation, outImage)
			
				
	return True

	
#Have the image name in the base folder as your Imprint Name
Generate_Imprint_Variation_Images(args["inputPath"], args["outputPath"], int(args["numberPill"]))


