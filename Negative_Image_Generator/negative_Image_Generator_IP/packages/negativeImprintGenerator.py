import cv2
import numpy as np
import string
import argparse
import random



'''
enum
{
    FONT_HERSHEY_SIMPLEX = 0,
    FONT_HERSHEY_PLAIN = 1,
    FONT_HERSHEY_DUPLEX = 2,
    FONT_HERSHEY_COMPLEX = 3,
    FONT_HERSHEY_TRIPLEX = 4,
    FONT_HERSHEY_COMPLEX_SMALL = 5,
    FONT_HERSHEY_SCRIPT_SIMPLEX = 6,
    FONT_HERSHEY_SCRIPT_COMPLEX = 7,
    FONT_ITALIC = 16
}
'''

class negativeImprintGenerator:
	def __init__ (self, mainImage, textLengthRange=(6,9), pixelMargin=10):

		self.mainImage = mainImage
		self.inputImage = self.removeImprint (self.mainImage)
		inputImageGray = cv2.cvtColor(self.inputImage, cv2.COLOR_BGR2GRAY)
		ret, mask_input = cv2.threshold( inputImageGray, 1, 255, 0 )

		kernel = np.ones( ( 3, 3 ), np.uint8 )
		image = cv2.erode( mask_input, kernel, iterations=3 )
		image = cv2.dilate( mask_input, kernel, iterations=3 )
		contours = cv2.findContours( image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE )[1]
		self.pillDims = cv2.boundingRect(contours[0]) # x,y,w,h
		self.pixelMargin = pixelMargin
		self.fontScaleList = [.4, .7, 1, 1.5, 2 ]
		self.thicknessList = [1, 2, 3] 
		self.textLengthList = list(np.arange(textLengthRange[0], textLengthRange[1]) )
		self.fontList = [cv2.FONT_HERSHEY_SIMPLEX,
			    cv2.FONT_HERSHEY_PLAIN,
			    cv2.FONT_HERSHEY_DUPLEX,
			    cv2.FONT_HERSHEY_COMPLEX,
			    cv2.FONT_HERSHEY_TRIPLEX,
			    cv2.FONT_HERSHEY_COMPLEX_SMALL,
			    cv2.FONT_ITALIC,
			    ]
		self.imprintList = str(string.ascii_letters[26:])+str(string.digits)+" "
	





	def createImage (self, blurredImage, pillImage):
		ret, mask = cv2.threshold( pillImage, 1, 255, 0 )
		blurredImage [mask == 0] = 0
		return blurredImage

	def pastingObject (self, bgImage, maskImage, extImage):
		maskImage = cv2.cvtColor(maskImage, cv2.COLOR_BGR2GRAY)
		ret, mask = cv2.threshold( maskImage, 1, 255, 0 )
		extImageAnd = cv2.bitwise_and(extImage, extImage, mask = mask)
		invertMask = (255-mask)
		andResult = cv2.bitwise_and(bgImage, bgImage, mask = invertMask)	
		orResult = andResult + extImageAnd
		return orResult
		
	def sp_noise(self, image,prob, mean_input):
		output = np.zeros(image.shape,np.uint8)
		thres = 1 - prob 
		for i in range(image.shape[0]):
			for j in range(image.shape[1]):
				rdn = random.random()
				if rdn < prob:
					output[i][j] = int(mean_input[0]+5)
				elif rdn > thres:
					output[i][j] = int(mean_input[0])
				else:
					output[i][j] = image[i][j]
		return output

	def removeImprint(self, img_in):		
		kernel = np.ones((5,5),np.uint8)
		img_in_copy = img_in.copy()
		inputImage = img_in.copy()
		img_in_gray = cv2.cvtColor(img_in, cv2.COLOR_BGR2GRAY)
		ret, mask_input = cv2.threshold( img_in_gray, 1, 255, 0 )

		mean_input = cv2.mean (img_in_gray, mask_input)

		blurredImage = img_in.copy()
		kernel_blur = np.array([[0,   0, 0, 1/4, 0, 0, 0  ],
					[0,   0, 0, 0,   0, 0, 0  ],
					[0,   0, 0, 0,   0, 0, 0  ],
					[1/4, 0, 0, 0,   0, 0, 1/4],
					[0,   0, 0, 0,   0, 0, 0  ],
					[0,   0, 0, 0,   0, 0, 0  ],
					[0,   0, 0, 1/4, 0, 0, 0  ]])
	
		for _ in range (10):
			blurredImage = cv2.filter2D(blurredImage, -1, kernel_blur)
			
		
		# Finding the imprint area
		img_in_gray = cv2.cvtColor(img_in_copy, cv2.COLOR_BGR2GRAY)
		adaptThresh = cv2.adaptiveThreshold(img_in_gray,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV,41,1)		
		mask_input_erose = cv2.erode(mask_input,kernel,iterations = 2)
		adaptThresh = cv2.bitwise_and(adaptThresh, adaptThresh, mask = mask_input_erose)
		adaptThresh_imprint = cv2.dilate(adaptThresh,kernel,iterations = 2)
	

		# Replacing imprint with the blurred image
		blurredImageSharp = self.createImage (blurredImage, img_in)
		img_in_copy[adaptThresh_imprint > 0] = blurredImageSharp[adaptThresh_imprint > 0]

		
		# Applying noise
		imprintImage = self.createImage (img_in_copy.copy(), adaptThresh_imprint)
		imprintImage = self.sp_noise(imprintImage,0.001, mean_input)		
		
			
		# Adding into the original image and returning back
		planeImageNoise = img_in_copy.copy()
		planeImageNoise[adaptThresh_imprint > 0] = imprintImage[adaptThresh_imprint > 0]

		for _ in range (4):
			planeImageNoise = cv2.blur (planeImageNoise, (3,3),0)	
		
		planeImageNoise = self.createImage (planeImageNoise, img_in)
				
		return planeImageNoise
		   

	
	def getNegImage (self, trueImprint):
		while True:
			text = None
			textLength = random.choice(self.textLengthList)
			for _ in range(textLength):
				l = random.choice((self.imprintList))
				if text is None:
					text = str(l)
				else:
					text = text+str(l)

			if trueImprint is text:
				continue

			HSV_inputImage = cv2.cvtColor(self.inputImage, cv2.COLOR_BGR2HSV)
			h_in,s_in,v_in = cv2.split(HSV_inputImage)

			b_img,g_img,r_img = cv2.split(self.inputImage)

			b_mean = b_img.mean()
			g_mean = g_img.mean()
			r_mean = r_img.mean()


			Temp = np.zeros((224, 224, 3), dtype="uint8")

			font = random.choice(self.fontList)
			fontScale = random.choice(self.fontScaleList)
			thickness = random.choice(self.thicknessList)
	
	
			# get boundary of this text
			textsize = cv2.getTextSize(text, font, fontScale, thickness)[0]
		
			if (self.pillDims[2]-self.pixelMargin < textsize[0] or self.pillDims[2]/2 > textsize[0]):
				continue
			textX = (Temp.shape[1] - textsize[0]) / 2
			textY = (Temp.shape[0] + textsize[1]) / 2

			# add text centered on image
			cv2.putText(Temp, text, (int(textX), int(textY) ), font, fontScale, (b_mean, g_mean, r_mean), thickness)
			HSV_imprint = cv2.cvtColor(Temp, cv2.COLOR_BGR2HSV)
			h,s,v = cv2.split(HSV_imprint)

			h = np.zeros((224, 224), dtype="uint8")
			s = np.zeros((224, 224), dtype="uint8")

			kernel_emboss_1 = np.array([[0,1,0],
						    [1,0,-1],
						    [0,-1,0]])

			kernel_emboss_2 = np.array([[0,-1,0],
						    [-1,0,1],
						    [0,1,0]])

			v_1 = cv2.filter2D(v, -1, kernel_emboss_1)
			v_2 = cv2.filter2D(v, -1, kernel_emboss_2)
					      
			v_temp = v_2 - v_1

			HSV_imprint = cv2.merge((h ,s, v_temp ))

			HSV_or = HSV_inputImage - HSV_imprint
			outImage = cv2.cvtColor(HSV_or, cv2.COLOR_HSV2BGR)
			return outImage
