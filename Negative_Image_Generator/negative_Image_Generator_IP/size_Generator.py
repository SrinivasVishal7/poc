import os
import cv2
import numpy as np
import argparse


original_images_parent_path = "E:/Work/0..DeepLearning/1.Tools/Tool_Creating_Dataset_Console/Tool_Creating_Dataset_Console/Negative_Image_Generator/Base_Images"
negative_images_parent_path   = "E:/Work/0..DeepLearning/1.Tools/Tool_Creating_Dataset_Console/Tool_Creating_Dataset_Console/Negative_Image_Generator/Output_Images"

property = "size"

Height = 224
Width = 224


#Filling the remaining area with 0's
def fix_img_size(image, img_size, channels=3):
	rows, cols = image.shape[:2]
	
	image_out = np.zeros((img_size, img_size, channels), dtype="uint8")

	shift_c = int((img_size - cols) / 2)
	shift_r = int((img_size - rows) / 2)

	image_out[shift_r: shift_r + rows, shift_c:shift_c + cols] = image[:, :]
	return image_out
	

# Function to generate different colors
def Generate_Size_Variation_Images(input_image_path, output_image_path, verbose=True):

	if not os.path.exists(input_image_path):
		print("Error : Input parent path is not found")
		return False
	

	for image_name in os.listdir(input_image_path):
	
		img = cv2.imread(os.path.join(input_image_path, image_name))
		image_name = image_name.split(".")[0]
		
		# Checking the input matrices
		if(img is None):
			print("Error : Input matrix is empty")
			return False
			
			
		output_image_directory = os.path.sep.join([output_image_path, property, image_name])		
		
		if not os.path.exists(output_image_directory):
			os.makedirs(output_image_directory)
			print(output_image_directory)
	
		
		for x in range(8, 13, 1):
			if(x != 10):			
				scale = x/10	
									
				if(x > 10):	
					res = cv2.resize(img,None,fx=scale, fy=scale, interpolation = cv2.INTER_CUBIC)
					
					res_x_cen = int(len(res[0])/2)
					res_y_cen = int(len(res)/2)

					center_crop = res[res_y_cen-int(Height/2):res_y_cen+int(Height/2), res_x_cen-int(Width/2):res_x_cen+int(Width/2)]
				
				else:
					res = cv2.resize(img,None,fx=scale, fy=scale, interpolation = cv2.INTER_AREA)					
					center_crop = fix_img_size(res, Width)				
				
				fileLocation = os.path.sep.join([output_image_directory, str(int(scale*100)) + ".png"])
				cv2.imwrite(fileLocation, center_crop)

				
	return True

	

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--inputPath", default=None, required=True,
	help="path that contains the input images")
ap.add_argument("-o", "--outputPath", default=None, required=True,
	help="path to save the negative generated images(Size)")
args = vars(ap.parse_args())


Generate_Size_Variation_Images(args["inputPath"], args["outputPath"])
#Generate_Size_Variation_Images(original_images_parent_path, negative_images_parent_path)
